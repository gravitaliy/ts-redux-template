import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import {Provider} from "react-redux";
import {createGlobalStyle} from "styled-components";

// local
import {store} from "./store";
import {HomePage} from "./pages/HomePage";
import {TodoPageContainer} from "./pages/TodoPage";
import {RoutesList} from "./assets/enums/RoutesList";
import {Navbar} from "./components/Navbar";


export const StyledGlobal = createGlobalStyle`
    body {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
        "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
        sans-serif;
    }
`


const App: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <StyledGlobal/>
                <Navbar/>
                <Switch>
                    <Route component={HomePage} path={RoutesList.HOME} exact/>
                    <Route component={TodoPageContainer} path={RoutesList.TODO}/>
                </Switch>
            </Router>
        </Provider>
    );
};


export default App;
