import React, {useEffect} from "react";

// local
import {useTypedSelector} from "../hooks/useTypedSelector";
import {fetchUsers} from "../store/action-creators/userActions";
import {useActions} from "../hooks/useActions";


interface UserListProps {
    users: any[];
}


const UserList: React.FC<UserListProps> = ({users}) => (
    <div>
        {users.map(user =>
            <div key={user.id}>{user.name}</div>
        )}
    </div>
);


export const UserListContainer: React.FC = () => {
    const {users, error, loading} = useTypedSelector(state => state.user);
    const {fetchUsers} = useActions();

    useEffect(() => {
        fetchUsers();
    }, []);

    if (loading) return <h1>Идет загрузка...</h1>

    if (error) return <h1>{error}</h1>

    return <UserList users={users}/>
};
