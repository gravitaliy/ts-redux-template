import React, {useEffect} from "react";
import styled from "styled-components/macro";

// local
import {useTypedSelector} from "../hooks/useTypedSelector";
import {useActions} from "../hooks/useActions";


interface TableListProps {
    page: number;
    todos: any[];
    pages: number[];
    handleSetTablePage: (page: number) => () => void;
}


interface PageStyledProps {
    isActivePage: boolean;
}


const PagesWrapStyled = styled.div`
     display: flex;
`;


const PageStyled = styled.div<PageStyledProps>`
      padding: 10px;
      cursor: pointer;
      border: ${({isActivePage}) => isActivePage ? "2px solid green" : "1px solid gray"};
`;


const TableList: React.FC<TableListProps> = ({page, todos, pages, handleSetTablePage}) => (
    <div>
        {todos.map(todo =>
            <div key={todo.id}>{todo.id} - {todo.title}</div>
        )}
        <PagesWrapStyled>
            {pages.map(p =>
                <PageStyled
                    key={p}
                    isActivePage={p === page}
                    onClick={handleSetTablePage(p)}
                >
                    {p}
                </PageStyled>
            )}
        </PagesWrapStyled>
    </div>
);


export const TableListContainer: React.FC = () => {
    const {page, error, loading, todos, limit} = useTypedSelector(state => state.todo)
    const {fetchTables, setTablePage} = useActions()
    const pages = [1, 2, 3, 4, 5]

    useEffect(() => {
        fetchTables(page, limit);
    }, [page, limit]);

    const handleSetTablePage = (page: number) => (): void => {
        setTablePage(page);
    }

    if (loading) return <h1>Идет загрузка...</h1>

    if (error) return <h1>{error}</h1>

    return <TableList page={page} todos={todos} pages={pages} handleSetTablePage={handleSetTablePage}/>
};
