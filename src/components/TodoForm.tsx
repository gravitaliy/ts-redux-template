import React, {useState} from "react";
import styled from "styled-components";


interface TodoFormContainerProps {
    onAdd: (title: string) => void;
}

interface TodoFormProps {
    title: string;
    handleInputChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
    handleAddItem: (event: React.FormEvent<HTMLFormElement>) => void;
}


const InputFieldStyled = styled.div`
    margin-top: 20px;
`;


const TodoForm: React.FC<TodoFormProps> = ({title, handleInputChange, handleAddItem}) => {
    return (
        <InputFieldStyled>
            <form onSubmit={handleAddItem}>
                <label htmlFor="title">Введите название дела</label><br/>
                <input
                    type="text"
                    id="title"
                    value={title}
                    onChange={handleInputChange}
                />
                <button type="submit">добавить</button>
            </form>
        </InputFieldStyled>
    );
}


export const TodoFormContainer: React.FC<TodoFormContainerProps> = ({onAdd}) => {
    const titleInitialState = "";
    const [title, setTitle] = useState<string>(titleInitialState);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        setTitle(event.target.value);
    }

    const handleAddItem = (): void => {
        onAdd(title);
        setTitle(titleInitialState);
    }

    return <TodoForm
        title={title}
        handleInputChange={handleInputChange}
        handleAddItem={handleAddItem}
    />
}
