import React from "react";
import {NavLink} from "react-router-dom";

//local
import {RoutesList} from "../assets/enums/RoutesList";


export const Navbar: React.FC = () => {
    return (
        <div>
            <h1>Navbar</h1>
            <ul>
                <li>
                    <NavLink to={RoutesList.HOME}>Тест таблиц</NavLink>
                </li>
                <li>
                    <NavLink to={RoutesList.TODO}>Тест todos</NavLink>
                </li>
            </ul>
            <hr/>
        </div>
    )
}
