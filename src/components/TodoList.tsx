import React from "react";
import styled, {css} from "styled-components/macro";

//local
import {ITodo} from "../assets/interfaces";


interface TodoListProps {
    todos: ITodo[];
    handleChangeToggle: (id: number) => () => void;
    handleRemove: (id: number) => (event: React.MouseEvent) => void;
}

interface TodoListContainerProps {
    todos: ITodo[];
    onRemove: (id: number) => void;
    onToggle(id: number): void; // или так
}

interface TodoListItemProps {
    completed: boolean;
}


const StyledTodoListItem = styled.li<TodoListItemProps>`
    margin-top: 20px;
  
    ${({completed}) => completed && css`
        color: red;
    `}
`;


const TodoList: React.FC<TodoListProps> = ({todos, handleChangeToggle, handleRemove}) => {
    return (
        <ul>
            {todos.map(todo => (
                <StyledTodoListItem completed={todo.completed} key={todo.id}>
                    <label>
                        <input
                            type="checkbox"
                            checked={todo.completed}
                            onChange={handleChangeToggle(todo.id)}
                        />
                        <span>{todo.title} - </span>
                        <i onClick={handleRemove(todo.id)}>[X]</i>
                    </label>
                </StyledTodoListItem>
            ))}
        </ul>
    )
}


export const TodoListContainer: React.FC<TodoListContainerProps> = ({todos, onToggle, onRemove}) => {
    const handleRemove = (id: number) => (event: React.MouseEvent): void => {
        event.preventDefault();
        onRemove(id);
    }

    const handleChangeToggle = (id: number) => (): void => {
        onToggle(id);
    }

    if (todos.length === 0) return <p>пока дел нет!</p>

    return <TodoList todos={todos} handleChangeToggle={handleChangeToggle} handleRemove={handleRemove} />;
}
