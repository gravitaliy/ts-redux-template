import React from "react";
import {UserListContainer} from "../components/UserList";
import {TableListContainer} from "../components/TableList";

export const HomePage: React.FC = () => {
    return (
        <div>
            <UserListContainer/>
            <hr/>
            <TableListContainer/>
        </div>
    );
};
