import React, {useEffect, useState} from "react";

//local
import {TodoFormContainer} from "../components/TodoForm";
import {TodoListContainer} from "../components/TodoList";
import {ITodo} from "../assets/interfaces";


declare var confirm: (question: string) => boolean;


interface TodoPageProps {
    todos: ITodo[];
    handleAdd: (title: string) => void;
    handleToggle: (id: number) => void;
    handleRemove: (id: number) => void;
}


const TodoPage: React.FC<TodoPageProps> = ({todos, handleAdd, handleToggle, handleRemove}) => {
    return (
        <>
            <TodoFormContainer onAdd={handleAdd}/>
            <TodoListContainer
                todos={todos}
                onToggle={handleToggle}
                onRemove={handleRemove}
            />
        </>
    );
}


export const TodoPageContainer: React.FC = () => {
    const [todos, setTodos] = useState<ITodo[]>([]);

    useEffect(() => {
        const saved = JSON.parse(localStorage.getItem('todos') || "[]") as ITodo[];
        setTodos(saved);
    }, []);

    useEffect(() => {
        localStorage.setItem("todos", JSON.stringify(todos));
    }, [todos]);

    const handleAdd = (title: string): void => {
        const newTodo: ITodo = {
            title,
            id: Date.now(),
            completed: false,
        }
        setTodos(prevState => [newTodo, ...prevState]);
    }

    const handleToggle = (id: number): void => {
        setTodos(todos.map(todo => {
            if (todo.id === id) {
                todo.completed = !todo.completed
            }
            return todo;
        }))
    }

    const handleRemove = (id: number): void => {
        const shoudRemove = confirm("Вы уверены, что хотите удалить элемент?");
        if (shoudRemove) {
            setTodos(prevState => prevState.filter(todo => todo.id !== id));
        }
    }

    return <TodoPage
        todos={todos}
        handleAdd={handleAdd}
        handleToggle={handleToggle}
        handleRemove={handleRemove}
    />
}
