import {TableActionTypes} from "../assets/enums/TableActionTypes";


export interface TableState {
    todos: any[];
    loading: boolean;
    error: null | string;
    page: number;
    limit: number;
}


export interface FetchTableAction {
    type: TableActionTypes.FETCH_TABLES;
    payload: {
        page: number;
        limit: number;
    };
}


interface FetchTableSuccessAction {
    type: TableActionTypes.FETCH_TABLES_SUCCESS;
    payload: any[];
}


interface FetchTableErrorAction {
    type: TableActionTypes.FETCH_TABLES_ERROR;
    payload: string;
}


interface SetTablePage {
    type: TableActionTypes.SET_TABLE_PAGE;
    payload: number;
}


export type TableAction = FetchTableAction | FetchTableErrorAction | FetchTableSuccessAction | SetTablePage;
