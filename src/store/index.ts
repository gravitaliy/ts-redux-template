import {applyMiddleware, createStore} from "redux";
import createSagaMiddleware from "redux-saga";
import {composeWithDevTools} from "redux-devtools-extension";

// local
import {rootReducer} from "./reducers";
import {rootWatcher} from "../saga";


const sagaMiddleware = createSagaMiddleware();


export const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(sagaMiddleware)
    )
);


sagaMiddleware.run(rootWatcher);
