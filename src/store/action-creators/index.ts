import * as UserActionCreators from "./userActions";
import * as TodoActionCreators from "./tableActions";


export default {
    ...TodoActionCreators,
    ...UserActionCreators
}
