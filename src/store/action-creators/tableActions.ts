import {TableAction} from "../../types/tableTypes";
import {TableActionTypes} from "../../assets/enums/TableActionTypes";


export const fetchTables = (page: number, limit: number): TableAction => ({
    type: TableActionTypes.FETCH_TABLES,
    payload: {page, limit},
});


export const fetchTablesSuccess = (payload: any[]): TableAction => ({
    type: TableActionTypes.FETCH_TABLES_SUCCESS,
    payload,
});


export const fetchTablesError = (): TableAction => ({
    type: TableActionTypes.FETCH_TABLES_ERROR,
    payload: 'Произошла ошибка при загрузке списка дел',
});


export const setTablePage = (page: number) => ({
    type: TableActionTypes.SET_TABLE_PAGE,
    payload: page,
});
