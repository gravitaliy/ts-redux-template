import {UserAction} from "../../types/userTypes";
import {UserActionTypes} from "../../assets/enums/UserActionTypes";


export const fetchUsers = (): UserAction => ({
    type: UserActionTypes.FETCH_USERS,
});


export const fetchUsersSuccess = (payload: any[]): UserAction => ({
    type: UserActionTypes.FETCH_USERS_SUCCESS,
    payload,
});


export const fetchUsersError = (): UserAction => ({
    type: UserActionTypes.FETCH_USERS_ERROR,
    payload: 'Произошла ошибка при загрузке пользователей',
});
