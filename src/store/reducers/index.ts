import {combineReducers} from "redux";

// local
import {userReducer} from "./userReducer";
import {tableReducer} from "./tableReducer";


export const rootReducer = combineReducers({
    user: userReducer,
    todo: tableReducer,
});


export type RootState = ReturnType<typeof rootReducer>;
