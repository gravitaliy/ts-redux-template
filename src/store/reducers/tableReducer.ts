import {TableAction, TableState} from "../../types/tableTypes";
import {TableActionTypes} from "../../assets/enums/TableActionTypes";


const initialState: TableState = {
    todos: [],
    page: 1,
    error: null,
    limit: 10,
    loading: false,
}


export const tableReducer = (state: TableState = initialState, action: TableAction): TableState => {
    switch (action.type) {
        case TableActionTypes.FETCH_TABLES:
            return {...state, loading: true};
        case TableActionTypes.FETCH_TABLES_SUCCESS:
            return {...state, loading: false, todos: action.payload};
        case TableActionTypes.FETCH_TABLES_ERROR:
            return {...state, loading: false, error: action.payload};
        case TableActionTypes.SET_TABLE_PAGE:
            return {...state, page: action.payload};
        default:
            return state;
    }
}
