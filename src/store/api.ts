import axios from "axios";


const api = "https://jsonplaceholder.typicode.com";


interface GetTablesTypes {
    _page: number;
    _limit: number;
}


export class Api {
    /*
      constructor() {
        axios.defaults.headers["X-Token"] =
          "99df42d5bd4e327b1719254214cf1a7daa602d7a26e881fdc1f8957aefb8c68c";
      }
    */

    static getUsers() {
        return axios.get(`${api}/users`);
    }

    static getTables(params: GetTablesTypes) {
        return axios.get(`${api}/todos`, {params});
    }
}
