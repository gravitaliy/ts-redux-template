import {put, takeEvery, call} from "redux-saga/effects";

// local
import {Api} from "../store/api";
import {FetchTableAction} from "../types/tableTypes";
import {fetchTablesError, fetchTablesSuccess} from "../store/action-creators/tableActions";
import {TableActionTypes} from "../assets/enums/TableActionTypes";


function* tableWorker(action: FetchTableAction) {
    const {page, limit} = action.payload;
    try {
        const params = {_page: page, _limit: limit};
        const {data} = yield call(Api.getTables, params);
        yield put(fetchTablesSuccess(data));
    } catch (error) {
        yield put(fetchTablesError());
    }
}


export function* tableWatcher() {
    yield takeEvery(TableActionTypes.FETCH_TABLES, tableWorker);
}
