import {all} from "redux-saga/effects";

// local
import {tableWatcher} from "./tableSaga";
import {userWatcher} from "./userSaga";


export function* rootWatcher() {
    yield all([userWatcher(), tableWatcher()]);
}
