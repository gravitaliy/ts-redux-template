import {put, takeEvery, call} from "redux-saga/effects";

// local
import {Api} from "../store/api";
import {fetchUsersSuccess, fetchUsersError} from "../store/action-creators/userActions";
import {UserActionTypes} from "../assets/enums/UserActionTypes";


function* userWorker() {
    try {
        const {data} = yield call(Api.getUsers);
        yield put(fetchUsersSuccess(data));
    } catch (error) {
        yield put(fetchUsersError());
    }
}


export function* userWatcher() {
    yield takeEvery(UserActionTypes.FETCH_USERS, userWorker);
}
