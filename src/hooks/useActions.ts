import {useDispatch} from "react-redux";
import {bindActionCreators} from "redux";

// local
import ActionCreators from '../store/action-creators/'


export const useActions = () => {
    const dispatch = useDispatch();
    return bindActionCreators(ActionCreators, dispatch);
}
