import {TypedUseSelectorHook, useSelector} from "react-redux";

// local
import {RootState} from "../store/reducers";


export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
