# Typescript + Redux template
Typescript + React + Redux + Redux Saga + Styled Components template

## Public urls


dev: [https://epic-wilson-43e8d2.netlify.app/](https://epic-wilson-43e8d2.netlify.app/)


## Backend url: no back

## Installation

Use the package manager [yarn](https://yarnpkg.com/) to install.

```bash
yarn
```

## Run app
```bash
yarn start
```

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

## Build

```bash
yarn build
```
